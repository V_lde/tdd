package iut.tdd;

public class Convert {
	public static String num2text(String input) {
		String res = "";
		int tailleNombre = input.length();
		String[] strChiffre = new String[]{"zéro", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf"};
		String[] strDizaine = new String[]{null, "dix", "vingt", "trente", "quarante", "cinquante", "soixante", "soixante-dix", "quatre-vingt", "quatre-vingt-dix"};
		String[] strCentaine = new String[]{null, "cent", "deux-cent", "trois-cent", "quatre-cent", "cinq-cent", "six-cent","sept-cent", "huit-cent", "neuf-cent"};
		char[] charChiffre = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		
		if(tailleNombre == 1) {
			for(int i = 0; i < 10; i++) {
				if(input.charAt(0) == charChiffre[i]) res += strChiffre[i];
			}
		}
		
		else if(tailleNombre == 2) {
			for(int i = 0; i < 10; i++) {
				if(input.charAt(0) == charChiffre[i]) res += strDizaine[i];
			}
		}
		
		else if(tailleNombre == 3) {
			for(int i = 0; i < 10; i++) {
				if(input.charAt(0) == charChiffre[i]) res += strCentaine[i];
			}
		}
		
		return res;
	}
	public static String text2num(String input) {
		return null;
	}
}